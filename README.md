# README #

This README would normally document whatever steps are necessary to get your application up and running.

### Key Functionality ###

* Access from Care Plan Step to launch Telehealth (with the new calendar view we can schedule these “appts” in a step)
* Notes from the call would be entered into the step or an Assessment.
* Will we need to create a SOAP format or initiate the call from within LB and document in the EMR.  (SSO will be needed).
* Do we need to keep a video recording?
* Need to be able to bring the details of the call back into the application (i.e. date, time started, time ended, duration, number called – need to explore the other options available)
* Ability to enter Diagnosis and CPT code for the telehealth visit (may be able to use Time Spent Entry for this) We need to look at the function of the time spent, right now I would tie it into an assessment question
* Billing File or Interface to send billing codes to EMR/PM system
* Integration of Note/Assessment into EMR (will require a document interface) 
* Integration of Appts from EMR to our Calendar View based on appointment type
* Integration of Appts from EMR into a view for the CM’s

### Microsoft Teams Application ###
https://portal.azure.com/#blade/Microsoft_AAD_RegisteredApps/ApplicationMenuBlade/Credentials/appId/bd22acbc-e10e-4054-81cf-3860a03940a6/isMSAApp/

* Name: Lightbeam Telehealth Prototype Application
* ClientId = bd22acbc-e10e-4054-81cf-3860a03940a6
* ClientSecret = t_-yk:]oAF9wJk85v347-ZPNrY]:vuXX
* RedirectUri = https://localhost:44360/MicrosoftTeams.aspx
* GraphScopes = "User.Read, User.ReadWrite, User.Read.All, Group.Read.All, GroupMember.Read.All, OnlineMeetings.ReadWrite, Calendars.ReadWrite"
* TenantId = b384c323-fd39-4008-b967-3fc84b0bc4ca

### API Endpoint Online Documentation ###
These endpoints are coded in the Test Harness

* Authorization API
https://docs.microsoft.com/en-us/graph/auth-v2-user
* Get User API
https://docs.microsoft.com/en-us/graph/api/user-get?
* Get user mailbox settings API
https://docs.microsoft.com/en-us/graph/api/user-get-mailboxsettings
* Create Event API (used to create online meeting)
https://docs.microsoft.com/en-us/graph/api/user-post-events
* Delete Event API (used to delete/cancel an online meeting)
https://docs.microsoft.com/en-us/graph/api/event-delete

### Microsoft Graph API Test Site ###
* https://developer.microsoft.com/en-us/graph/graph-explorer#

