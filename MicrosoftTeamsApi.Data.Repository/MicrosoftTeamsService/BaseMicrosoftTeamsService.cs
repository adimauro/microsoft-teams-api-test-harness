﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using MicrosoftTeams.Api.Helpers;
using MicrosoftTeams.Authentication.Data.Models;
using MicrosoftTeams.Shared.Data.Models;


namespace MicrosoftTeams.Data.Repository
{
    public class BaseMicrosoftTeamsServiceAsync
    { 
        // The base URIs for the Produciton and Sandbox Environments
        private const string PRODUCTION_BASE_URI = @"https://graph.microsoft.com";

        protected HttpClient HttpClient { get; set; }

        /// <summary>
        /// Initialises a new Microsoft Teams Service base class with the required configurations
        /// </summary>
        /// <param name="token">The access token</param>
        /// <param name="useSandboxEnvironment">Use the produciton environment or development sandbox environment. Defaults to Production Environment.</param>
        public BaseMicrosoftTeamsServiceAsync(AccessTokenType tokenType, string token)
        {
            if (string.IsNullOrWhiteSpace(token)) throw new ArgumentException("Parameter is required", nameof(token));

            this.HttpClient = new HttpClient
            {
                BaseAddress = new Uri(PRODUCTION_BASE_URI)
            };

            var authenticationScheme = tokenType == AccessTokenType.Server ? "Token" : "Bearer";
            this.HttpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(authenticationScheme, token);

            // Set accept headers to JSON only
            this.HttpClient.DefaultRequestHeaders.Accept.Clear();
            this.HttpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

    }
}