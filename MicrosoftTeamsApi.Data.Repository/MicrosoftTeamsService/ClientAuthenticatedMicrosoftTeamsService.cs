﻿using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

using MicrosoftTeams.Api.Helpers;
using MicrosoftTeams.Authentication.Data.Models;

using MicrosoftTeams.Shared.Data.Models;
using MicrosoftTeams.Data.Models;
using System;

namespace MicrosoftTeams.Data.Repository
{
    public class ClientAuthenticatedMicrosoftTeamsServiceAsync : BaseMicrosoftTeamsServiceAsync
    {
        private readonly string _version = "v1.0";

        /// <summary>
        /// Initialises a new Uber Health Service object with the required configurations
        /// </summary>
        /// <param name="accessToken">The access token</param>
        /// <param name="useSandboxEnvironment">Use the produciton environment or development sandbox environment. Defaults to Production Environment.</param>
        public ClientAuthenticatedMicrosoftTeamsServiceAsync(string accessToken)
            : base(AccessTokenType.Client, accessToken)
        { }



        #region [User Methods]

        /// <summary>
        /// This endpoint returns information about the user that has been authorized with the application.
        /// </summary>
        /// <returns>Returns a User Profile object</returns>
        public async Task<MicrosoftTeamsResponse<User>> GetUserAsync()
        {
            var url = $"/{this._version}/me";

            return await this.HttpClient.HttpGetAsync<User>(url);
        }


        /// <summary>
        /// This endpoint endpoint returns information about the user specified by the id
        /// </summary>
        /// <param name="userId">Id of the User</param>
        /// <returns>Returns a User Profile object</returns>
        public async Task<MicrosoftTeamsResponse<User>> GetUserAsync(Guid userId)
        {
            var url = $"/{this._version}/users/{userId}";

            return await this.HttpClient.HttpGetAsync<User>(url);
        }


        /// <summary>
        /// This endpoint returns mailbox settings for the user that has been authorized with the application
        /// </summary>
        /// <returns>Returns a User Mailbox Settings object</returns>
        public async Task<MicrosoftTeamsResponse<MailboxSettings>> GetUserMailboxSettingsAsync()
        {
            var url = $"/{this._version}/me/mailboxSettings";

            return await this.HttpClient.HttpGetAsync<MailboxSettings>(url);
        }


        /// <summary>
        /// This endpoint returns mailbox settings for the  about the user specified by the id
        /// </summary>
        /// <param name="userId">Id of the User</param>
        /// <returns>Returns a User Mailbox Settings object</returns>
        public async Task<MicrosoftTeamsResponse<MailboxSettings>> GetUserMailboxSettingsAsync(Guid userId)
        {
            var url = $"/{this._version}/users/{userId}/mailboxSettings";

            return await this.HttpClient.HttpGetAsync<MailboxSettings>(url);
        }


        /// <summary>
        /// This endpoint returns a list of Users in Teams
        /// </summary>
        /// <returns>Returns a Users Collection object</returns>
        public async Task<MicrosoftTeamsResponse<UserCollection>> GetUsersAsync()
        {
            var url = $"/{this._version}/users";

            return await this.HttpClient.HttpGetAsync<UserCollection>(url);
        }

        #endregion



        #region [Team Methods]

        /// <summary>
        /// This endpoint returns the Microsoft Teams that the user is a direct member of.
        /// </summary>
        /// <returns>Returns a UserProfile object</returns>
        public async Task<MicrosoftTeamsResponse<TeamCollection>> GetUserTeamsAsync()
        {
            var url = $"/{this._version}/me/joinedTeams";

            return await this.HttpClient.HttpGetAsync<TeamCollection>(url);
        }


        /// <summary>
        /// This endpoint returns the Microsoft Teams that the user is a direct member of.
        /// </summary>
        /// <param name="userId">Id of the User</param>
        /// <returns>Returns a UserProfile object</returns>
        public async Task<MicrosoftTeamsResponse<TeamCollection>> GetUserTeamsAsync(Guid userId)
        {
            var url = $"/{this._version}/users/{userId}/joinedTeams";

            return await this.HttpClient.HttpGetAsync<TeamCollection>(url);
        }

        #endregion



        #region [Event/Meeting Methods]

        /// <summary>
        /// This endpoint returns Creates a Microsoft Teams meeting with the specified data
        /// </summary>
        /// <param name="userId">id of the User creating the meeting</param>
        /// <param name="subject">Subject of the Meeting</param>
        /// <param name="messageBody">Message Body of the Meeting</param>
        /// <param name="location">the name of the location meeting</param>
        /// <param name="timeZone">name of the Time Zone (i.e. this value would be something like "Eastern Time Zone") should be retrieved from users Mailbox Settings</param>
        /// <param name="startTime">Starting date and time of the meeting</param>
        /// <param name="endTime">Ending date and time of the meeting</param>
        /// <param name="attendees">List of Attendees invited to the meeting</param>
        /// <returns>Returns a Meeting response object</returns>
        public async Task<MicrosoftTeamsResponse<Meeting>> CreateMeetingAsync(Guid userId, string subject, string messageBody, string location, string timeZone, DateTime startTime, DateTime endTime, List<Attendee> attendees)
        {
            var url = $"/{this._version}/users/{userId}/events";

            MeetingRequest meetingRequest = new MeetingRequest();
            meetingRequest.IsOnlineMeeting = true;
            meetingRequest.OnlineMeetingProvider = "teamsForBusiness";
            meetingRequest.Subject = subject;
            meetingRequest.Body = new MessageBody("HTML", messageBody);
            meetingRequest.Location = new MeetingLocation(location);
            meetingRequest.StartTime = new ScheduledDateTime(string.Format("{0:yyyy-MM-ddTHH:mm:ss.FFF}", startTime), timeZone);
            meetingRequest.EndTime = new ScheduledDateTime(string.Format("{0:yyyy-MM-ddTHH:mm:ss.FFF}", endTime), timeZone);

            List<MeetingAttendee> meetingAttendees = new List<MeetingAttendee>();
            foreach (Attendee attendee in attendees)
            {
                MeetingAttendee meetingAttendee = new MeetingAttendee();
                meetingAttendee.Type = (attendee.Required ? "required" : "optional");
                meetingAttendee.EmailAddress = new EmailAddress(attendee.Name, attendee.Email);
                meetingAttendees.Add(meetingAttendee);
            }
            meetingRequest.Attendees = meetingAttendees.ToArray();

            JsonSerializerSettings jsonSerializerSettings = new JsonSerializerSettings();
            jsonSerializerSettings.NullValueHandling = NullValueHandling.Ignore;
            //string json = JsonConvert.SerializeObject(jsonObject, jsonSerializerSettings);
            string json = JsonConvert.SerializeObject(meetingRequest, jsonSerializerSettings);
            var payload = new StringContent(json, Encoding.UTF8, "application/json");

            return await this.HttpClient.HttpPostAsync<Meeting>(url, payload);
        }


        /// <summary>
        /// This endpoint returns Creates a Microsoft Teams meeting with the specified data
        /// </summary>
        /// <param name="userPrincipalName">Principal Name of the User creating the meeting, usually the user's email address</param>
        /// <param name="subject">Subject of the Meeting</param>
        /// <param name="messageBody">Message Body of the Meeting</param>
        /// <param name="location">the name of the location meeting</param>
        /// <param name="timeZone">name of the Time Zone (i.e. this value would be something like "Eastern Time Zone") should be retrieved from users Mailbox Settings</param>
        /// <param name="startTime">Starting date and time of the meeting</param>
        /// <param name="endTime">Ending date and time of the meeting</param>
        /// <param name="attendees">List of Attendees invited to the meeting</param>
        /// <returns>Returns a Meeting response object</returns>
        public async Task<MicrosoftTeamsResponse<Meeting>> CreateMeetingAsync(string userPrincipalName, string subject, string messageBody, string location, string timeZone, DateTime startTime, DateTime endTime, List<Attendee> attendees)
        {
            var url = $"/{this._version}/users/{userPrincipalName}/events";

            MeetingRequest meetingRequest = new MeetingRequest();
            meetingRequest.IsOnlineMeeting = true;
            meetingRequest.OnlineMeetingProvider = "teamsForBusiness";
            meetingRequest.Subject = subject;
            meetingRequest.Body = new MessageBody("HTML", messageBody);
            meetingRequest.Location = new MeetingLocation(location);
            meetingRequest.StartTime = new ScheduledDateTime(string.Format("{0:yyyy-MM-ddTHH:mm:ss.FFF}", startTime), timeZone);
            meetingRequest.EndTime = new ScheduledDateTime(string.Format("{0:yyyy-MM-ddTHH:mm:ss.FFF}", endTime), timeZone);

            List<MeetingAttendee> meetingAttendees = new List<MeetingAttendee>();
            foreach (Attendee attendee in attendees)
            {
                MeetingAttendee meetingAttendee = new MeetingAttendee();
                meetingAttendee.Type = (attendee.Required ? "required" : "optional");
                meetingAttendee.EmailAddress = new EmailAddress(attendee.Name, attendee.Email);
                meetingAttendees.Add(meetingAttendee);
            }
            meetingRequest.Attendees = meetingAttendees.ToArray();

            JsonSerializerSettings jsonSerializerSettings = new JsonSerializerSettings();
            jsonSerializerSettings.NullValueHandling = NullValueHandling.Ignore;
            //string json = JsonConvert.SerializeObject(jsonObject, jsonSerializerSettings);
            string json = JsonConvert.SerializeObject(meetingRequest, jsonSerializerSettings);
            var payload = new StringContent(json, Encoding.UTF8, "application/json");

            return await this.HttpClient.HttpPostAsync<Meeting>(url, payload);
        }


        /// <summary>
        /// This endpoint deletes the specified meeting
        /// </summary>
        /// <param name="userId">id of the User who created the meeting</param>
        /// <param name="meetingId">The id of the meeting to delete</param>
        /// <returns>Returns a result of delete meeting request</returns>
        public async Task<MicrosoftTeamsResponse<bool>> DeleteMeetingAsync(Guid userId, string meetingId)
        {
            var url = $"/users/{userId}/events/{meetingId}";

            return await this.HttpClient.HttpDeleteAsync(url);
        }
        #endregion

    }

}