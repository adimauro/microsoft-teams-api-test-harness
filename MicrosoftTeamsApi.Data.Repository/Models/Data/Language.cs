﻿using Newtonsoft.Json;
using System;

namespace MicrosoftTeams.Data.Models
{
    public partial class Language
    {
        [JsonProperty("locale")]
        public string Locale { get; set; }

        [JsonProperty("displayName")]
        public string DisplayName { get; set; }
    }
}
