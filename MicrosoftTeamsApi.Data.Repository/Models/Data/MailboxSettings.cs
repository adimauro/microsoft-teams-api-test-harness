﻿using Newtonsoft.Json;
using System;

namespace MicrosoftTeams.Data.Models
{
    public partial class MailboxSettings
    {
        [JsonProperty("@odata.context")]
        public Uri OdataContext { get; set; }

        [JsonProperty("automaticRepliesSetting")]
        public AutomaticRepliesSetting AutomaticRepliesSetting { get; set; }

        [JsonProperty("timeZone")]
        public string TimeZone { get; set; }

        [JsonProperty("language")]
        public Language Language { get; set; }

        [JsonProperty("workingHours")]
        public WorkingHours WorkingHours { get; set; }

        [JsonProperty("dateFormat")]
        public string DateFormat { get; set; }

        [JsonProperty("timeFormat")]
        public string TimeFormat { get; set; }

        [JsonProperty("delegateMeetingMessageDeliveryOptions")]
        public string DelegateMeetingMessageDeliveryOptions { get; set; }
    }
}
