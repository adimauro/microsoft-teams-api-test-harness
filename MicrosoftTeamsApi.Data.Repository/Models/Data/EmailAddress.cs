﻿using Newtonsoft.Json;
using System;

namespace MicrosoftTeams.Data.Models
{
    public partial class EmailAddress
    {
        public EmailAddress(string name, string email)
        {
            Name = name;
            Email = email;
        }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("address")]
        public string Email { get; set; }
    }
}
