﻿using Newtonsoft.Json;
using System;

namespace MicrosoftTeams.Data.Models
{
    //public partial class ScheduledDateTime
    //{
    //    public ScheduledDateTime(DateTimeOffset dateTime, string timeZone)
    //    {
    //        DateTime = dateTime;
    //        TimeZone = timeZone;
    //    }

    //    [JsonProperty("dateTime")]
    //    public DateTimeOffset DateTime { get; set; }

    //    [JsonProperty("timeZone")]
    //    public string TimeZone { get; set; }
    //}

    public partial class ScheduledDateTime
    {
        public ScheduledDateTime(string dateTime, string timeZone)
        {
            DateTime = dateTime;
            TimeZone = timeZone;
        }

        [JsonProperty("dateTime")]
        public string DateTime { get; set; }

        [JsonProperty("timeZone")]
        public string TimeZone { get; set; }
    }

}
