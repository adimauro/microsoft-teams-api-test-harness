﻿using Newtonsoft.Json;
using System;

namespace MicrosoftTeams.Data.Models
{
    public partial class TimeZone
    {
        [JsonProperty("name")]
        public string Name { get; set; }
    }
}
