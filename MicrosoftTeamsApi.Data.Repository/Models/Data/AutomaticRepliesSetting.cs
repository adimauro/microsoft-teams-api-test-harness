﻿using Newtonsoft.Json;
using System;

namespace MicrosoftTeams.Data.Models
{
    public partial class AutomaticRepliesSetting
    {
        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("externalAudience")]
        public string ExternalAudience { get; set; }

        [JsonProperty("scheduledStartDateTime")]
        public ScheduledDateTime ScheduledStartDateTime { get; set; }

        [JsonProperty("scheduledEndDateTime")]
        public ScheduledDateTime ScheduledEndDateTime { get; set; }

        [JsonProperty("internalReplyMessage")]
        public string InternalReplyMessage { get; set; }

        [JsonProperty("externalReplyMessage")]
        public string ExternalReplyMessage { get; set; }
    }

}
