﻿using Newtonsoft.Json;
using System;

namespace MicrosoftTeams.Data.Models
{
    public partial class OnlineMeeting
    {
        [JsonProperty("joinUrl")]
        public Uri JoinUrl { get; set; }

        [JsonProperty("conferenceId")]
        public long ConferenceId { get; set; }

        [JsonProperty("tollNumber")]
        public string TollNumber { get; set; }
    }
}
