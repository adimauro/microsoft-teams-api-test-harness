﻿using Newtonsoft.Json;
using System;

namespace MicrosoftTeams.Data.Models
{
    public partial class WorkingHours
    {
        [JsonProperty("daysOfWeek")]
        public string[] DaysOfWeek { get; set; }

        [JsonProperty("startTime")]
        public DateTimeOffset StartTime { get; set; }

        [JsonProperty("endTime")]
        public DateTimeOffset EndTime { get; set; }

        [JsonProperty("timeZone")]
        public TimeZone TimeZone { get; set; }
    }
}
