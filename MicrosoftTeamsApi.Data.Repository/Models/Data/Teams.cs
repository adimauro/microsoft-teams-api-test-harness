﻿using Newtonsoft.Json;
using System;

namespace MicrosoftTeams.Data.Models
{
    public partial class TeamCollection
    {
        [JsonProperty("@odata.context")]
        public Uri OdataContext { get; set; }

        [JsonProperty("@odata.count")]
        public long OdataCount { get; set; }

        [JsonProperty("value")]
        public Team[] Teams { get; set; }
    }

    public partial class Team
    {
        [JsonProperty("id")]
        public Guid Id { get; set; }

        [JsonProperty("displayName")]
        public string DisplayName { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("internalId")]
        public object InternalId { get; set; }

        [JsonProperty("classification")]
        public object Classification { get; set; }

        [JsonProperty("specialization")]
        public object Specialization { get; set; }

        [JsonProperty("visibility")]
        public object Visibility { get; set; }

        [JsonProperty("webUrl")]
        public object WebUrl { get; set; }

        [JsonProperty("isArchived")]
        public bool IsArchived { get; set; }

        [JsonProperty("memberSettings")]
        public object MemberSettings { get; set; }

        [JsonProperty("guestSettings")]
        public object GuestSettings { get; set; }

        [JsonProperty("messagingSettings")]
        public object MessagingSettings { get; set; }

        [JsonProperty("funSettings")]
        public object FunSettings { get; set; }

        [JsonProperty("discoverySettings")]
        public object DiscoverySettings { get; set; }
    }
}
