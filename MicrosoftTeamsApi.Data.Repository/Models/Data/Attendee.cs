﻿using Newtonsoft.Json;
using System;

namespace MicrosoftTeams.Data.Models
{
    public partial class Attendee
    {
        public Attendee(string name, string email, bool required)
        {
            Name = name;
            Email = email;
            Required = required;
        }

        public string Name { get; set; }
        public string Email { get; set; }
        public bool Required { get; set; }
    }

}
