﻿using Newtonsoft.Json;

namespace MicrosoftTeams.Data.Models
{
    public class UserCollection
    {
        [JsonProperty("value")]
        public User[] Users { get; set; }
    }


    public class User
    {
        [JsonProperty(PropertyName = "businessPhones")]
        public string[] BusinessPhones { get; set; }

        [JsonProperty(PropertyName = "displayName")]
        public string DisplayName { get; set; }

        [JsonProperty(PropertyName = "givenName")]
        public string GivenName { get; set; }

        [JsonProperty(PropertyName = "jobTitle")]
        public string JobTitle { get; set; }

        [JsonProperty(PropertyName = "mail")]
        public string Mail { get; set; }

        [JsonProperty(PropertyName = "mobilePhone")]
        public string MobilePhone { get; set; }

        [JsonProperty(PropertyName = "officeLocation")]
        public string OfficeLocation { get; set; }

        [JsonProperty(PropertyName = "preferredLanguage")]
        public string PreferredLanguage { get; set; }

        [JsonProperty(PropertyName = "surname")]
        public string Surname { get; set; }

        [JsonProperty(PropertyName = "userPrincipalName")]
        public string UserPrincipalName { get; set; }

        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }

    }

}
