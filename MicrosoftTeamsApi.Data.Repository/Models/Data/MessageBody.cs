﻿using Newtonsoft.Json;
using System;

namespace MicrosoftTeams.Data.Models
{
    public partial class MessageBody
    {
        public MessageBody(string contentType, string messageBody)
        {
            ContentType = contentType;
            Content = messageBody;
        }


        [JsonProperty("contentType")]
        public string ContentType { get; set; }

        [JsonProperty("content")]
        public string Content { get; set; }
    }
}
