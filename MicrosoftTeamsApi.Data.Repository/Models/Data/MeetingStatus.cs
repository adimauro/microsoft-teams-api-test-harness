﻿using Newtonsoft.Json;
using System;

namespace MicrosoftTeams.Data.Models
{
    public partial class MeetingStatus
    {
        [JsonProperty("response")]
        public string Response { get; set; }

        [JsonProperty("time")]
        public DateTimeOffset Time { get; set; }
    }
}
