﻿using Newtonsoft.Json;
using System;

namespace MicrosoftTeams.Data.Models
{
    public partial class MeetingAttendee
    {
        //The attendee type: required, optional, resource.
        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("status")]
        public MeetingStatus Status { get; set; }

        [JsonProperty("emailAddress")]
        public EmailAddress EmailAddress { get; set; }
    }

}
