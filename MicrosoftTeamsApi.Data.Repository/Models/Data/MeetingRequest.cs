﻿using Newtonsoft.Json;
using System;

namespace MicrosoftTeams.Data.Models
{

    public partial class MeetingRequest
    {
        [JsonProperty("subject")]
        public string Subject { get; set; }

        [JsonProperty("body")]
        public MessageBody Body { get; set; }

        [JsonProperty("start")]
        public ScheduledDateTime StartTime { get; set; }

        [JsonProperty("end")]
        public ScheduledDateTime EndTime { get; set; }

        [JsonProperty("location")]
        public MeetingLocation Location { get; set; }

        [JsonProperty("attendees")]
        public MeetingAttendee[] Attendees { get; set; }

        [JsonProperty("allowNewTimeProposals")]
        public bool AllowNewTimeProposals { get; set; }

        [JsonProperty("isOnlineMeeting")]
        public bool IsOnlineMeeting { get; set; }

        [JsonProperty("onlineMeetingProvider")]
        public string OnlineMeetingProvider { get; set; }
    }
}


