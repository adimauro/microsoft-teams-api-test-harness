﻿using Newtonsoft.Json;
using System;

namespace MicrosoftTeams.Data.Models
{
    public partial class MeetingCollection
    {
        [JsonProperty("@odata.context")]
        public Uri OdataContext { get; set; }

        [JsonProperty("@odata.nextLink")]
        public Uri OdataNextLink { get; set; }

        [JsonProperty("value")]
        public Meeting[] Meetings { get; set; }
    }


    public partial class Meeting
    {
        [JsonProperty("@odata.context")]
        public Uri OdataContext { get; set; }

        [JsonProperty("@odata.etag")]
        public string OdataEtag { get; set; }

        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("createdDateTime")]
        public DateTimeOffset CreatedDateTime { get; set; }

        [JsonProperty("lastModifiedDateTime")]
        public DateTimeOffset LastModifiedDateTime { get; set; }

        [JsonProperty("changeKey")]
        public string ChangeKey { get; set; }

        [JsonProperty("categories")]
        public object[] Categories { get; set; }

        [JsonProperty("originalStartTimeZone")]
        public string OriginalStartTimeZone { get; set; }

        [JsonProperty("originalEndTimeZone")]
        public string OriginalEndTimeZone { get; set; }

        [JsonProperty("uid")]
        public string Uid { get; set; }

        [JsonProperty("reminderMinutesBeforeStart")]
        public long ReminderMinutesBeforeStart { get; set; }

        [JsonProperty("isReminderOn")]
        public bool IsReminderOn { get; set; }

        [JsonProperty("hasAttachments")]
        public bool HasAttachments { get; set; }

        [JsonProperty("subject")]
        public string Subject { get; set; }

        [JsonProperty("bodyPreview")]
        public string BodyPreview { get; set; }

        [JsonProperty("importance")]
        public string Importance { get; set; }

        [JsonProperty("sensitivity")]
        public string Sensitivity { get; set; }

        [JsonProperty("isAllDay")]
        public bool IsAllDay { get; set; }

        [JsonProperty("isCancelled")]
        public bool IsCancelled { get; set; }

        [JsonProperty("isDraft")]
        public bool IsDraft { get; set; }

        [JsonProperty("isOrganizer")]
        public bool IsOrganizer { get; set; }

        [JsonProperty("responseRequested")]
        public bool ResponseRequested { get; set; }

        [JsonProperty("seriesMasterId")]
        public object SeriesMasterId { get; set; }

        [JsonProperty("showAs")]
        public string ShowAs { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("webLink")]
        public Uri WebLink { get; set; }

        [JsonProperty("onlineMeetingUrl")]
        public object OnlineMeetingUrl { get; set; }

        [JsonProperty("isOnlineMeeting")]
        public bool IsOnlineMeeting { get; set; }

        [JsonProperty("onlineMeetingProvider")]
        public string OnlineMeetingProvider { get; set; }

        [JsonProperty("allowNewTimeProposals")]
        public bool AllowNewTimeProposals { get; set; }

        [JsonProperty("responseStatus")]
        public MeetingStatus MeetingStatus { get; set; }

        [JsonProperty("body")]
        public MessageBody Body { get; set; }

        [JsonProperty("start")]
        public ScheduledDateTime StartTime { get; set; }

        [JsonProperty("end")]
        public ScheduledDateTime EndTime { get; set; }

        [JsonProperty("location")]
        public MeetingLocation Location { get; set; }

        [JsonProperty("locations")]
        public MeetingLocation[] Locations { get; set; }

        [JsonProperty("recurrence")]
        public object Recurrence { get; set; }

        [JsonProperty("attendees")]
        public MeetingAttendee[] Attendees { get; set; }

        [JsonProperty("organizer")]
        public MeetingOrganizer Organizer { get; set; }

        [JsonProperty("onlineMeeting")]
        public OnlineMeeting OnlineMeeting { get; set; }
    }
}
