﻿using Newtonsoft.Json;
using System;

namespace MicrosoftTeams.Data.Models
{
    public partial class MeetingOrganizer
    {
        [JsonProperty("emailAddress")]
        public EmailAddress EmailAddress { get; set; }
    }
}
