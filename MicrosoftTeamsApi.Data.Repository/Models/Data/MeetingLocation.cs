﻿using Newtonsoft.Json;
using System;

namespace MicrosoftTeams.Data.Models
{
    public partial class MeetingLocation
    {
        public MeetingLocation (string displayName)
        {
            DisplayName = displayName;
        }

        [JsonProperty("displayName")]
        public string DisplayName { get; set; }

        [JsonProperty("locationType", NullValueHandling = NullValueHandling.Ignore)]
        public string LocationType { get; set; }

        [JsonProperty("uniqueId", NullValueHandling = NullValueHandling.Ignore)]
        public string UniqueId { get; set; }

        [JsonProperty("uniqueIdType", NullValueHandling = NullValueHandling.Ignore)]
        public string UniqueIdType { get; set; }
    }
}
