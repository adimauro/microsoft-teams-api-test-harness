﻿namespace MicrosoftTeams.Authentication.Data.Models
{
    public enum AccessTokenType
    {
        Server,
        Client
    }
}