﻿using System;
using Newtonsoft.Json;


namespace MicrosoftTeams.Shared.Data.Models
{
    public partial class ErrorResponse
    {
        [JsonProperty("error")]
        public Error Error { get; set; }
    }

    public partial class Error
    {
        [JsonProperty("code")]
        public string Code { get; set; }

        [JsonProperty("message")]
        public string Message { get; set; }

        [JsonProperty("innerError")]
        public InnerError InnerError { get; set; }
    }

    public partial class InnerError
    {
        [JsonProperty("request-id")]
        public System.Guid RequestId { get; set; }

        [JsonProperty("date")]
        public DateTimeOffset Date { get; set; }
    }
}