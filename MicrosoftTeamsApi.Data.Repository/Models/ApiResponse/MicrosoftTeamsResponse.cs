﻿
namespace MicrosoftTeams.Shared.Data.Models
{
    public class MicrosoftTeamsResponse<T>
    {
        public T Data { get; set; }
        public ErrorResponse ErrorResponse { get; set; }
    }
}