﻿
namespace MicrosoftTeams.Shared.Data.Models
{
    /// <summary>
    /// Public interface for an Api Exception
    /// </summary>
    public class MicrosoftTeamsException : System.Exception
    {
        #region Constructor
        /// <summary>
        /// Default constructor
        /// </summary>
        public MicrosoftTeamsException() { }

        /// <summary>
        /// Default constructor
        /// </summary>
        public MicrosoftTeamsException(string code, string message)
        {
            Code = code;
            Message = message;
        }
        #endregion
        public string Code { get; set; }

        public string Message { get; set; }
    }
}