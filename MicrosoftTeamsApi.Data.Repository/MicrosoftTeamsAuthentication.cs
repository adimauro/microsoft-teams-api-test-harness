﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using Newtonsoft.Json;
using System.Threading.Tasks;
using System.Net.Http;
using System.Text;

using MicrosoftTeams.Authentication.Data.Models;
using MicrosoftTeams.Api.Helpers;


namespace MicrosoftTeams.Authentication.Data.Repository
{
    public class MicrosoftTeamsAuthenticationAsync
    {
        private readonly string _version = "v2.0";
        private readonly string _clientId;
        private readonly string _clientSecret;

        /// <summary>
        /// Initialises a new Authenitcation Object"/>
        /// </summary>
        /// <param name="clientId">The client ID assigned by Microsoft Teams found in the Microsoft Teams Application Dashboard</param>
        /// <param name="clientSecret">The client secret assigned by Microsoft Teams found in the Microsoft Teams Application Dashboard</param>
        public MicrosoftTeamsAuthenticationAsync(string clientId, string clientSecret)
        {
            if (string.IsNullOrWhiteSpace(clientId)) throw new ArgumentException("Parameter is required", nameof(clientId));
            if (string.IsNullOrWhiteSpace(clientSecret)) throw new ArgumentException("Parameter is required", nameof(clientSecret));

            this._clientId = clientId;
            this._clientSecret = clientSecret;
        }


        /// <summary>
        /// Generates an OAuth authorization URL based on scopes, state and redirect.
        /// </summary>
        /// <param name="scopes">
        /// Space delimited list of grant scopes you would like to have permission to access on behalf of the user.
        /// If none is provided the default is the set selected in your application’s dashboard.
        /// It is invalid to provide no scopes and have none selected in the dashboard.
        /// </param>
        /// <param name="state">State which will be passed back to you to prevent tampering.</param>
        /// <param name="redirectUri">The URI redirect back to after authorization by the resource owner. Defaults to Redirect URI in the Microsoft Teams App Dashboard if not provided.
        /// </param>
        /// <returns>Returns the OAuth authorization URL.</returns>
        public string GetAuthorizeUrl(List<string> scopes = null, string state = null, string redirectUri = null)
        {
            var queryParameters = new Dictionary<string, string>
            {
                { "client_id", this._clientId },
                { "response_type", "code" },
                { "response_mode", "query" }
            };

            var graphScopes = "openid email profile offline_access";
            if (scopes != null && scopes.Any())
            {
                graphScopes += " " + string.Join(" ", scopes);
            }
            queryParameters.Add("scope", graphScopes);

            if (!string.IsNullOrWhiteSpace(state))
            {
                queryParameters.Add("state", state);
            }

            if (!string.IsNullOrWhiteSpace(redirectUri))
            {
                queryParameters.Add("redirect_uri", redirectUri);
            }

            var encodedQueryParameters = MicrosoftTeamsApiHelpersAsync.FormatUrlEncodedQueryParameters(queryParameters);
            var authorizeUrl = @"https://login.microsoftonline.com/common/oauth2/" + $"{this._version}/authorize" + "?" + encodedQueryParameters;

            return authorizeUrl;
        }



        /// <summary>
        /// Gets the authorization page for login
        /// </summary>
        /// <param name="scopes">
        /// Space delimited list of grant scopes you would like to have permission to access on behalf of the user.
        /// If none is provided the default is the set selected in your application’s dashboard.
        /// It is invalid to provide no scopes and have none selected in the dashboard.
        /// </param>
        /// <param name="state">State which will be passed back to you to prevent tampering.</param>
        /// <param name="redirectUri">The URI redirect back to after authorization by the resource owner. Defaults to Redirect URI in the Microsoft Teams App Dashboard if not provided.
        /// <returns>Returns the OAuth authorization URL</returns>
        public async Task<string> GetAuthorizationAsync(List<string> scopes = null, string state = null, string redirectUri = null)
        {
            string authorizationPage = string.Empty;

            var queryParameters = new Dictionary<string, string>
            {
                { "client_id", this._clientId },
                { "response_type", "code" },
                { "response_mode", "query" }
            };

            var graphScopes = string.Empty;
            if (scopes != null && scopes.Any())
            {
                graphScopes = string.Join(" ", scopes);
            }
            queryParameters.Add("scope", "openid email profile offline_access " + graphScopes);

            if (!string.IsNullOrWhiteSpace(state))
            {
                queryParameters.Add("state", state);
            }

            if (!string.IsNullOrWhiteSpace(redirectUri))
            {
                queryParameters.Add("redirect_uri", redirectUri);
            }

            var encodedQueryParameters = MicrosoftTeamsApiHelpersAsync.FormatUrlEncodedQueryParameters(queryParameters);
            var authorizeUrl = @"https://login.microsoftonline.com/common/oauth2/" + $"{this._version}/authorize" + "?" + encodedQueryParameters;

            try
            {
                using (var httpClient = new HttpClient())
                {
                    var response = await httpClient
                        .GetAsync(authorizeUrl)
                        .ConfigureAwait(false);

                    if (response.IsSuccessStatusCode)
                    {
                        var responseContent = await response.Content.ReadAsStringAsync();
                        authorizationPage = JsonConvert.DeserializeObject<string>(responseContent);
                    }
                }
            }
            catch (WebException ex)
            {
                //string message = ((HttpWebResponse)ex.Response).StatusDescription;
                //HttpStatusCode code = ((HttpWebResponse)ex.Response).StatusCode;

                var webResponse = new StreamReader(ex.Response.GetResponseStream()).ReadToEnd();
                //microsoft TeamsResponse.Error = JsonConvert.DeserializeObject<Microsoft TeamsError>(webResponse);
            }

            return authorizationPage;
        }



        /// <summary>
        /// Get Access Token using Authorization Code Grant Type and will exchange the authorization code for an AccessToken
        /// </summary>
        /// <param name="authorizationCode">The authorization code</param>
        /// <param name="scopes">List of Scopes account is Authorized to use</param>
        /// <param name="redirectUri">The URI redirect back to after authorization by the resource owner. Defaults to Redirect URI in the Microsoft Teams App Dashboard if not provided.
        /// <returns>Returns an AccessToken to be used in cliennt authenticated API requests</returns>
        public async Task<AccessToken> GetAccessTokenAsync(string authorizationCode, List<string> scopes = null, string redirectUri = null)
        {
            var queryParameters = new Dictionary<string, string>
            {
                { "client_id", this._clientId },
                { "client_secret", this._clientSecret },
                { "grant_type", "authorization_code" },
                { "code", authorizationCode },
            };

            var graphScopes = string.Empty;
            if (scopes != null && scopes.Any())
            {
                graphScopes = string.Join(" ", scopes);
            }
            queryParameters.Add("scope", "openid email profile offline_access " + graphScopes);

            if (!string.IsNullOrWhiteSpace(redirectUri))
            {
                queryParameters.Add("redirect_uri", redirectUri);
            }

            var tokenUrl = @"https://login.microsoftonline.com/common/oauth2/" + $"{this._version}/token";

            return await AuthorizeAsync(tokenUrl, queryParameters);
        }


        /// <summary>
        /// Refresh the Access Token retrieved on authorization if it has passed expiration
        /// </summary>
        /// <param name="refreshToken">Access Token to be refreshed</param>
        /// <param name="redirectUri">The URL the user should be redrected back to</param>
        /// <returns>Returns an AccessToken</returns>
        public async Task<AccessToken> RefreshAccessTokenAsync(string refreshToken, string redirectUri)
        {
            var queryParameters = new Dictionary<string, string>
            {
                { "client_id", this._clientId },
                { "client_secret", this._clientSecret },
                { "grant_type", "refresh_token" },
                { "refresh_token", refreshToken }
            };
            
            var tokenUrl = "https://login.microsoftonline.com/common/oauth2/" + $"{this._version}/token";

            return await AuthorizeAsync(tokenUrl, queryParameters);
        }



        /// <summary>
        /// Revoke a user's access to the Microsoft Teams API via the application.
        /// </summary>
        /// <param name="accessToken">The access token being revoked</param>
        /// <returns>Returns a boolean indicating if the Microsoft Teams API returned a successful HTTP status</returns>
        public async Task<AccessToken> RevokeAccessTokenAsync(string accessToken)
        {
            var queryParameters = new Dictionary<string, string>
            {
                { "client_id", this._clientId },
                { "client_secret", this._clientSecret },
                { "token", accessToken }
            };

            var revokeUrl = "https://login.microsoftonline.com/common/oauth2/" + $"{this._version}/revoke";

            return await AuthorizeAsync(revokeUrl, queryParameters);
        }



        /// <summary>
        /// Authorizes a client with Microsoft Teams OAuth2
        /// </summary>
        /// <param name="queryParameters">The Query Params for the Authorzition post call</param>
        /// <returns>Returns AccessToken if authorization is successful, Null if authorization fails</returns>
        private async Task<AccessToken> AuthorizeAsync(string url, Dictionary<string, string> queryParameters)
        {
            var accessToken = new AccessToken();

            var payload = new FormUrlEncodedContent(queryParameters);

            try
            {
                using (var httpClient = new HttpClient())
                {
                    var response = await httpClient
                        .PostAsync(url, payload)
                        .ConfigureAwait(false);

                    if (response.IsSuccessStatusCode)
                    {
                        var responseContent = await response.Content.ReadAsStringAsync();
                        accessToken = JsonConvert.DeserializeObject<AccessToken>(responseContent);
                    }
                }
            }
            catch (WebException ex)
            {
                //string message = ((HttpWebResponse)ex.Response).StatusDescription;
                //HttpStatusCode code = ((HttpWebResponse)ex.Response).StatusCode;

                var webResponse = new StreamReader(ex.Response.GetResponseStream()).ReadToEnd();
                //microsoft TeamsResponse.Error = JsonConvert.DeserializeObject<Microsoft TeamsError>(webResponse);
            }

            return accessToken;
        }



    }
}