﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using Newtonsoft.Json;

using MicrosoftTeams.Shared.Data.Models;


namespace MicrosoftTeams.Api.Helpers
{

    public static class MicrosoftTeamsApiHelpersAsync
    {
        /// <summary>
        /// Makes a GET request.
        /// </summary>
        /// <typeparam name="T">The response data type</typeparam>
        /// <param name="url">The URL being requested</param>
        /// <optional param name="queryParameters">The content being POST-ed</param>
        /// <returns>Returns object type T</returns>
        public static async Task<MicrosoftTeamsResponse<T>> HttpGetAsync<T>(this HttpClient httpClient, string url, Dictionary<string, string> queryParameters = null)
        {
            var microsoftTeamsResponse = new MicrosoftTeamsResponse<T>();

            if (queryParameters != null)
            {
                var encodedQueryParameters = FormatUrlEncodedQueryParameters(queryParameters);
                url += string.IsNullOrEmpty(encodedQueryParameters) ? string.Empty : "?" + encodedQueryParameters;
            }

            var response = await httpClient
                .GetAsync(url)
                .ConfigureAwait(false);
            try
            {
                var responseContent = await response.Content.ReadAsStringAsync();
                if (response.IsSuccessStatusCode)
                {
                    microsoftTeamsResponse.Data = JsonConvert.DeserializeObject<T>(responseContent);
                }
                else
                {
                    microsoftTeamsResponse.ErrorResponse = JsonConvert.DeserializeObject<ErrorResponse>(responseContent);
                }
            }
            catch (WebException ex)
            {
                //string message = ((HttpWebResponse)ex.Response).StatusDescription;
                //HttpStatusCode code = ((HttpWebResponse)ex.Response).StatusCode;

                var webResponse = new StreamReader(ex.Response.GetResponseStream()).ReadToEnd();
                microsoftTeamsResponse.ErrorResponse = JsonConvert.DeserializeObject<ErrorResponse>(webResponse);
            }

            return microsoftTeamsResponse;
        }



        /// <summary>
        /// Makes a POST request.
        /// </summary>
        /// <typeparam name="T">The response data type</typeparam>
        /// <param name="url">The URL being requested</param>
        /// <param name="queryParameters">The content being POST-ed</param>
        /// <returns>Returns object type T</returns>
        public static async Task<MicrosoftTeamsResponse<T>> HttpPostAsync<T>(this HttpClient httpClient, string url, StringContent payload)
        {
            var microsoftTeamsResponse = new MicrosoftTeamsResponse<T>();

            var response = await httpClient
                .PostAsync(url, payload)
                .ConfigureAwait(false);

            try
            {
                var responseContent = await response.Content.ReadAsStringAsync();
                if (response.IsSuccessStatusCode)
                {
                    microsoftTeamsResponse.Data = JsonConvert.DeserializeObject<T>(responseContent);
                }
                else
                {
                    microsoftTeamsResponse.ErrorResponse = JsonConvert.DeserializeObject<ErrorResponse>(responseContent);
                }
            }
            catch (WebException ex)
            {
                //string message = ((HttpWebResponse)ex.Response).StatusDescription;
                //HttpStatusCode code = ((HttpWebResponse)ex.Response).StatusCode;

                var webResponse = new StreamReader(ex.Response.GetResponseStream()).ReadToEnd();
                microsoftTeamsResponse.ErrorResponse = JsonConvert.DeserializeObject<ErrorResponse>(webResponse);
            }

            return microsoftTeamsResponse;
        }



        /// <summary>
        /// Makes a PUT request.
        /// </summary>
        /// <typeparam name="T">The response data type</typeparam>
        /// <param name="url">The URL being requested</param>
        /// <param name="queryParameters">The content being PATCH-ed</param>
        /// <returns>Returns object type T></returns>
        public static async Task<MicrosoftTeamsResponse<T>> HttpPutAsync<T>(this HttpClient httpClient, string url, StringContent payload)
        {
            var microsoftTeamsResponse = new MicrosoftTeamsResponse<T>();
            var response = await httpClient.PutAsync(url, payload).ConfigureAwait(false);

            try
            {
                var responseContent = await response.Content.ReadAsStringAsync();
                if (response.IsSuccessStatusCode)
                {
                    microsoftTeamsResponse.Data = JsonConvert.DeserializeObject<T>(responseContent);
                }
                else
                {
                    microsoftTeamsResponse.ErrorResponse = JsonConvert.DeserializeObject<ErrorResponse>(responseContent);
                }
            }
            catch (WebException ex)
            {
                //string message = ((HttpWebResponse)ex.Response).StatusDescription;
                //HttpStatusCode code = ((HttpWebResponse)ex.Response).StatusCode;

                var webResponse = new StreamReader(ex.Response.GetResponseStream()).ReadToEnd();
                microsoftTeamsResponse.ErrorResponse = JsonConvert.DeserializeObject<ErrorResponse>(webResponse);
            }

            return microsoftTeamsResponse;
        }



        /// <summary>
        /// Makes a PATCH request.
        /// </summary>
        /// <typeparam name="T">The response data type</typeparam>
        /// <param name="url">The URL being requested</param>
        /// <param name="queryParameters">The content being PATCH-ed</param>
        /// <returns>Returns object type T</returns>
        public static async Task<MicrosoftTeamsResponse<T>> HttpPatchAsync<T>(this HttpClient httpClient, string url, StringContent payload)
        {
            var microsoftTeamsResponse = new MicrosoftTeamsResponse<T>();

            var request = new HttpRequestMessage(new HttpMethod("PATCH"), url)
            {
                Content = payload
            };
            var response = await httpClient.SendAsync(request);

            try
            {
                var responseContent = await response.Content.ReadAsStringAsync();
                if (response.IsSuccessStatusCode)
                {
                    microsoftTeamsResponse.Data = JsonConvert.DeserializeObject<T>(responseContent);
                }
                else
                {
                    microsoftTeamsResponse.ErrorResponse = JsonConvert.DeserializeObject<ErrorResponse>(responseContent);
                }
            }
            catch (WebException ex)
            {
                //string message = ((HttpWebResponse)ex.Response).StatusDescription;
                //HttpStatusCode code = ((HttpWebResponse)ex.Response).StatusCode;

                var webResponse = new StreamReader(ex.Response.GetResponseStream()).ReadToEnd();
                microsoftTeamsResponse.ErrorResponse = JsonConvert.DeserializeObject<ErrorResponse>(webResponse);
            }

            return microsoftTeamsResponse;
        }



        /// <summary>
        /// Makes a DELETE request.
        /// </summary>
        /// <param name="url">The URL being requested</param>
        /// <returns>Returns a boolean indicating if the Uber API returned a successful HTTP status</returns>
        public static async Task<MicrosoftTeamsResponse<bool>> HttpDeleteAsync(this HttpClient httpClient, string url)
        {
            var microsoftTeamsResponse = new MicrosoftTeamsResponse<bool>();
            var response = await httpClient.DeleteAsync(url).ConfigureAwait(false);

            try
            {
                var responseContent = await response.Content.ReadAsStringAsync();
                if (response.IsSuccessStatusCode)
                {
                    microsoftTeamsResponse.Data = JsonConvert.DeserializeObject<bool>(responseContent);
                }
                else
                {
                    microsoftTeamsResponse.ErrorResponse = JsonConvert.DeserializeObject<ErrorResponse>(responseContent);
                }
            }
            catch (WebException ex)
            {
                //string message = ((HttpWebResponse)ex.Response).StatusDescription;
                //HttpStatusCode code = ((HttpWebResponse)ex.Response).StatusCode;

                var webResponse = new StreamReader(ex.Response.GetResponseStream()).ReadToEnd();
                microsoftTeamsResponse.ErrorResponse = JsonConvert.DeserializeObject<ErrorResponse>(webResponse);
            }

            return microsoftTeamsResponse;
        }



        public static string FormatUrlEncodedQueryParameters(Dictionary<string, string> queryParameters)
        {
            string encodedQueryParameters = "";
            if (queryParameters != null)
            {
                foreach (string key in queryParameters.Keys)
                {
                    encodedQueryParameters += string.IsNullOrEmpty(encodedQueryParameters) ? string.Empty : "&";
                    encodedQueryParameters += HttpUtility.UrlEncode(key) + "=" + HttpUtility.UrlEncode(queryParameters[key]);
                }
            }

            return encodedQueryParameters;
        }


    }
}
