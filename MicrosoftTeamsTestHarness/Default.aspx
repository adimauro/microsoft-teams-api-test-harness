﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="MicrosoftTeamsTestHarness.Default"   %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Microsoft Teams API Test Harness</title>
      <meta name="viewport" content="width=device-width, initial-scale=1" />
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" />
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <style type="text/css">

    </style>
</head>



<script>



</script>  


<body>
<div class="container">
    <form id="form1" runat="server">
        <div class="col-lg-12   pt-4">
			<h5 style="box-sizing: border-box; margin: 0px 0px 10px; padding: 0px; line-height: 1.25; font-family: ff-clan-web-pro, &quot;Helvetica Neue&quot;, Helvetica, sans-serif; font-weight: 400; color: rgb(0, 0, 0); font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;">Click to Begin Microsoft Teams Authentication Workflow</h5>

			<asp:Button ID="AuthenticationButton" runat="server" class="btn btn-primary" Text="Microsoft Teams Authentication" OnClick="AuthenticationRequest_Click" />
        </div>
    </form>
</div>


</body>
</html>
