﻿<%@ Page Language="C#" AutoEventWireup="true"  Async="true" MaintainScrollPositionOnPostback="true"  CodeBehind="UberHealth.aspx.cs" Inherits="MicrosoftTeamsTestHarness.MicrosoftTeams"   %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Microsoft Teams API Test Harness</title>
      <meta name="viewport" content="width=device-width, initial-scale=1" />
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" />
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <style type="text/css">


    </style>
</head>

<script>



</script>
<body>
<div class="container-fluid">

    <form id="form1" runat="server">
        <div class="pl-5 pr-5">
            <br />
            <h2 style="box-sizing: border-box; font-variant-ligatures: normal; font-variant-caps: normal; orphans: 2; widows: 2; -webkit-text-stroke-width: 0px; text-decoration-style: initial; text-decoration-color: initial;">Authentication</h2>
            <div class="form-row">
                <div class="col-md-12">
                    <asp:Label ID="AutheticationCodeLabel" runat="server" Text="Authetication Code"></asp:Label>
                    <asp:TextBox ID="AutheticationCodeTextBox" runat="server" class="form-control"></asp:TextBox>
		        </div>
		    </div> 
            <div class="form-row">
                <div class="col-md-12">
                    <asp:Label ID="AccessTokenLabel" runat="server" Text="Access Token"></asp:Label>
                    <asp:TextBox ID="AccessTokenTextBox" runat="server" class="form-control"></asp:TextBox>
		        </div>
		    </div>


		    <div class="form-row pt-2">
                <div class="col-md-5">
                    <asp:Label ID="UserProfileLabel" runat="server" Text="User Profile"></asp:Label>
                    <asp:TextBox ID="UserProfileTextBox" runat="server" ReadOnly="True" Rows="8" TextMode="MultiLine" class="form-control"></asp:TextBox>
                    <asp:Button ID="GetUserProfileButton" runat="server" class="btn btn-secondary" Text="Get User" OnClick="GetUserProfileButton_Click" />
                </div>
                <div class="col-md-2">
                </div>
                <div class="col-md-5">
                    <asp:Label ID="UserMailboxSettingsLabel" runat="server" Text="Mailbox Settings"></asp:Label>
                    <asp:TextBox ID="UserMailboxSettingsTextBox" runat="server" ReadOnly="True" Rows="8" TextMode="MultiLine" class="form-control"></asp:TextBox>
                </div>
		    </div>


		    <div class="form-row pt-4">
                <div class="col-md-4">
                    <asp:Label ID="CreateMeetingLabel" runat="server" Text="Create Teams Meeting"></asp:Label>

			        <div class="pt-2">
                        <asp:Label ID="UserIdLabel" runat="server" Text="User Id"></asp:Label>
                        <asp:TextBox ID="UserIdTextBox" runat="server" class="form-control"></asp:TextBox>
		            </div>

		            <div class="pt-2">
                        <asp:Label ID="UserTimeZoneLabel" runat="server" Text="User Time Zone"></asp:Label>
                        <asp:TextBox ID="UserTimeZoneTextBox" runat="server" class="form-control"></asp:TextBox>
		            </div>

                    <div class="pt-2">
                        <asp:Button ID="CreateMeetingButton" runat="server" class="btn btn-secondary" Text="Create Meeting" OnClick="CreateMeetingButton_Click" />
                    </div>

                    <div class="pt-4">
                        <asp:Label ID="MeetingResponseLabel" runat="server" Text="Meeting Response"></asp:Label>
                        <asp:TextBox ID="MeetingResponseTextBox" runat="server" ReadOnly="True" Rows="8" TextMode="MultiLine" class="form-control"></asp:TextBox>
                    </div>
                </div>
		    </div>


        </div>
    </form>
</div>
    <br /><br /><br /><br />
</body>
</html>
