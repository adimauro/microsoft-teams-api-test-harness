﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using MicrosoftTeams.Authentication.Data.Models;


namespace MicrosoftTeamsTestHarness
{
    public class MicrosoftTeamsDataRecord
    {
        public static string CLIENT_ID = ConfigurationManager.AppSettings["MicrosoftTeams:ClientId"];
        public static string CLIENT_SECRET = ConfigurationManager.AppSettings["MicrosoftTeams:ClientSecret"];
        public static string TENANT = ConfigurationManager.AppSettings["MicrosoftTeams:TenantId"];
        private static string REDIRECT_URL = ConfigurationManager.AppSettings["MicrosoftTeams:RedirectUri"];
        private static string SCOPES = ConfigurationManager.AppSettings["MicrosoftTeams:GraphScopes"];


        public MicrosoftTeamsDataRecord()
        {
            ClientId = CLIENT_ID;
            ClientSecret = CLIENT_SECRET;
            Scopes = SCOPES;
            RedirectUrl = REDIRECT_URL;
        }
        
        public string AuthenticationCode { get; set; }

        public  AccessToken AccessToken { get; set; }
        public string ClientId { get; set; }
        public string ClientSecret { get; set; }
        public string Scopes { get; set; }
        public string RedirectUrl { get; set; }




    }
}