﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Threading.Tasks;
using System.Web.UI;

using MicrosoftTeams.Data.Repository;
using MicrosoftTeams.Authentication.Data.Repository;
using Newtonsoft.Json;
using System.Net.Http;
using MicrosoftTeams.Data.Models;

namespace MicrosoftTeamsTestHarness
{




    public partial class MicrosoftTeams : System.Web.UI.Page
    {




        MicrosoftTeamsDataRecord _microsoftTeamsDataRecord;


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
            }

            string authenticationCode = Request.QueryString["code"];


            if (HttpContext.Current.Cache["MicrosoftTeamsDataRecord"] != null)
            {
                _microsoftTeamsDataRecord = (MicrosoftTeamsDataRecord)HttpContext.Current.Cache["MicrosoftTeamsDataRecord"];
            }
            else
            {
                _microsoftTeamsDataRecord = new MicrosoftTeamsDataRecord();
            }

            if (_microsoftTeamsDataRecord.AccessToken == null)
            {
                RegisterAsyncTask(new PageAsyncTask(GetAccessTokenAsync));
            }
            else
            {
                AutheticationCodeTextBox.Text = _microsoftTeamsDataRecord.AuthenticationCode;
                AccessTokenTextBox.Text = _microsoftTeamsDataRecord.AccessToken.Value;
            }

            _microsoftTeamsDataRecord.AuthenticationCode = authenticationCode;
            AutheticationCodeTextBox.Text = _microsoftTeamsDataRecord.AuthenticationCode;
            HttpContext.Current.Cache["MicrosoftTeamsDataRecord"] = _microsoftTeamsDataRecord;
        }



        protected void GetUserProfileButton_Click(object sender, EventArgs e)
        {
            //CreateSandboxRun();
            RegisterAsyncTask(new PageAsyncTask(GetUserProfileAsync));

        }

        
        protected void GetUsersButton_Click(object sender, EventArgs e)
        {
            //CreateSandboxRun();
            RegisterAsyncTask(new PageAsyncTask(GetUsersAsync));

        }



        protected void CreateMeetingButton_Click(object sender, EventArgs e)
        {
            //CreateSandboxRun();
            RegisterAsyncTask(new PageAsyncTask(CreateMeetingAsync));

        }






        #region [Uber Async Methods]

        private async Task GetAccessTokenAsync()
        {
            var scopesSettings = _microsoftTeamsDataRecord.Scopes.Split(',');

            var scopes = new List<string>();
            for (int ix = 0; ix < scopesSettings.Length; ix++)
            {
                scopes.Add(scopesSettings[ix].Trim());
            }

            var microsoftTeams = new MicrosoftTeamsAuthenticationAsync(_microsoftTeamsDataRecord.ClientId, _microsoftTeamsDataRecord.ClientSecret);
            var accessTokenTask = microsoftTeams.GetAccessTokenAsync(_microsoftTeamsDataRecord.AuthenticationCode, scopes, _microsoftTeamsDataRecord.RedirectUrl);
            var accessToken = await accessTokenTask;

            //Save Session Data
            _microsoftTeamsDataRecord.AccessToken = accessToken;
            HttpContext.Current.Cache["MicrosoftTeamsDataRecord"] = _microsoftTeamsDataRecord;

            AccessTokenTextBox.Text = accessToken.Value;

        }


        private async Task GetUserProfileAsync()
        {
            var microsoftTeamsService = new ClientAuthenticatedMicrosoftTeamsServiceAsync(_microsoftTeamsDataRecord.AccessToken.Value);

            var getUserProfileTask = microsoftTeamsService.GetUserAsync();
            var getMailboxSettingsTask = microsoftTeamsService.GetUserMailboxSettingsAsync();
            await Task.WhenAll(getUserProfileTask, getMailboxSettingsTask);


            var userProfile = await getUserProfileTask;
            var mailboxSettings = await getMailboxSettingsTask;



            if (userProfile.ErrorResponse != null)
            {
                UserProfileTextBox.Text =
                $"Error Getting User Profile" + Environment.NewLine +
                $"Code: {userProfile.ErrorResponse.Error.Code} {Environment.NewLine}" +
                $"Message: {userProfile.ErrorResponse.Error.Message} {Environment.NewLine}";
                UserIdTextBox.Text = string.Empty;
            }
            else
            {
                //_microsoftTeamsDataRecord.RunId = microsoftTeamsData.Data.DisplayName;
                //UserProfileTextBox.Text = $"Id: {userProfile.Data.Id} {Environment.NewLine}" +
                //                    $"DisplayName: {userProfile.Data.DisplayName} {Environment.NewLine}" +
                //                    $"GivenName: {userProfile.Data.GivenName} {Environment.NewLine}" +
                //                    $"Surname: {userProfile.Data.Surname} {Environment.NewLine}" +
                //                    $"JobTitle: {userProfile.Data.JobTitle} {Environment.NewLine}" +
                //                    $"Mail: {userProfile.Data.Mail} {Environment.NewLine}" +
                //                    $"MobilePhone: {userProfile.Data.MobilePhone} {Environment.NewLine}" +
                //                    $"OfficeLocation: {userProfile.Data.OfficeLocation} {Environment.NewLine}";


                JsonSerializerSettings jsonSerializerSettings = new JsonSerializerSettings();
                jsonSerializerSettings.NullValueHandling = NullValueHandling.Ignore;
                jsonSerializerSettings.Formatting = Formatting.Indented;
                UserProfileTextBox.Text = JsonConvert.SerializeObject(userProfile, jsonSerializerSettings);
                UserIdTextBox.Text = userProfile.Data.Id;
            }


            if (mailboxSettings.ErrorResponse != null)
            {
                UserMailboxSettingsTextBox.Text =
                $"Error Getting Mail Box Settings" + Environment.NewLine +
                $"Code: {mailboxSettings.ErrorResponse.Error.Code} {Environment.NewLine}" +
                $"Message: {mailboxSettings.ErrorResponse.Error.Message} {Environment.NewLine}";
                UserTimeZoneTextBox.Text = "Eastern Time Zone";
            }
            else

            {
                JsonSerializerSettings jsonSerializerSettings = new JsonSerializerSettings();
                jsonSerializerSettings.NullValueHandling = NullValueHandling.Ignore;
                jsonSerializerSettings.Formatting = Formatting.Indented;
                UserMailboxSettingsTextBox.Text = JsonConvert.SerializeObject(mailboxSettings, jsonSerializerSettings);
                UserTimeZoneTextBox.Text = mailboxSettings.Data.TimeZone;
            }



        }





        private async Task GetUsersAsync()
        {
            //var microsoftTeamsService = new ClientAuthenticatedMicrosoftTeamsServiceAsync(_microsoftTeamsDataRecord.AccessToken.Value);

            //var microsoftTeamsTask = microsoftTeamsService.GetUsersAsync();
            //var microsoftTeamsData = await microsoftTeamsTask;

            //if (microsoftTeamsData.ErrorResponse != null)
            //{
            //    UsersTextBox.Text =
            //    $"Code: {microsoftTeamsData.ErrorResponse.Error.Code} {Environment.NewLine}" +
            //    $"Message: {microsoftTeamsData.ErrorResponse.Error.Message} {Environment.NewLine}";
            //}
            //else if (microsoftTeamsData.Data != null)
            //{
            //    UsersTextBox.Text = $"Number of Users: {microsoftTeamsData.Data.Users.Length}{Environment.NewLine}{Environment.NewLine}";
            //    for (int ix = 0; ix < microsoftTeamsData.Data.Users.Length; ix++)
            //    {
            //        int userNumber = ix + 1;
            //        UsersTextBox.Text += $"User #: {userNumber} {Environment.NewLine}" +
            //                             $"DisplayName: {microsoftTeamsData.Data.Users[ix].DisplayName} {Environment.NewLine}" +
            //                              $"GivenName: {microsoftTeamsData.Data.Users[ix].GivenName} {Environment.NewLine}" +
            //                              $"Surname: {microsoftTeamsData.Data.Users[ix].Surname} {Environment.NewLine}" +
            //                              $"Id: {microsoftTeamsData.Data.Users[ix].Id} {Environment.NewLine}" +
            //                              $"JobTitle: {microsoftTeamsData.Data.Users[ix].JobTitle} {Environment.NewLine}" +
            //                              $"Mail: {microsoftTeamsData.Data.Users[ix].Mail} {Environment.NewLine}" +
            //                              $"MobilePhone: {microsoftTeamsData.Data.Users[ix].MobilePhone} {Environment.NewLine}" +
            //                              $"OfficeLocation: {microsoftTeamsData.Data.Users[ix].OfficeLocation} {Environment.NewLine}  {Environment.NewLine}";
            //    }
            //}
        }



        private async Task CreateMeetingAsync()
        {
            //Create Test Data
            Guid userId = new Guid(UserIdTextBox.Text);
            string subject = "Test Microsoft Teams Meeting";
            string message = "This is a test meeting for creating a Microsoft Teams meeting via the API";
            string location = "Online Teams Meeting";
            DateTime startTime = new DateTime(2020, 06, 1, 10, 00, 00, DateTimeKind.Local);
            DateTime endTime = new DateTime(2020, 06, 1, 12, 00, 00, DateTimeKind.Local);
            string timeZone = UserTimeZoneTextBox.Text; // "Eastern Standard Time";
            

            List<Attendee> attendeeList = new List<Attendee>();
            attendeeList.Add(new Attendee("Anthony DiMauro", "adimauro@lightbeamhealth.com", true));


            var microsoftTeamsService = new ClientAuthenticatedMicrosoftTeamsServiceAsync(_microsoftTeamsDataRecord.AccessToken.Value);

            var createMeetingTask = microsoftTeamsService.CreateMeetingAsync(userId, subject, message, location, timeZone, startTime, endTime, attendeeList);
            var meetingResponse = await createMeetingTask;


            JsonSerializerSettings jsonSerializerSettings = new JsonSerializerSettings();
            jsonSerializerSettings.NullValueHandling = NullValueHandling.Ignore;
            jsonSerializerSettings.Formatting = Formatting.Indented;
            MeetingResponseTextBox.Text = JsonConvert.SerializeObject(meetingResponse, jsonSerializerSettings);

        }






        #endregion




        #region [Page Control Helper Functions]


        //private void LoadProductEstimates(List<ProductEstimate> uberProductEstimates)
        //{
        //    //Clear the controls in the Documents Section
        //    divProductEstimates.Controls.Clear();

        //    HtmlGenericControl ul = new HtmlGenericControl("ul");
        //    ul.Attributes.Add("class", "list-group");

        //    int productEstimateCount = 0;
        //    foreach (var uberProductEstimate in uberProductEstimates)
        //    {
        //        if (uberProductEstimate.EstimateInfo.NoCarsAvailable == null)
        //        {
        //            productEstimateCount++;
        //            string idRadioButton = "RadioButton" + productEstimateCount.ToString();

        //            //Add new Document Subcategory Header and start a new List
        //            HtmlGenericControl li = new HtmlGenericControl("li");
        //            li.Attributes.Add("class", "list-group-item d-flex");

        //            HtmlGenericControl divUberProductSelect = new HtmlGenericControl("div");
        //            divUberProductSelect.Attributes.Add("class", "mr-4");
        //            HtmlInputRadioButton radioButton = new HtmlInputRadioButton();
        //            radioButton.Attributes.Add("type", "radio");
        //            radioButton.Attributes.Add("id", idRadioButton);
        //            radioButton.Attributes.Add("name", "RadioButtonList");
        //            radioButton.Attributes.Add("value", uberProductEstimate.Product.ProductId);
        //            radioButton.Attributes.Add("key", uberProductEstimate.Product.ProductId);
        //            divUberProductSelect.Controls.Add(radioButton);


        //            HtmlGenericControl divUberProduct = new HtmlGenericControl("div");
        //            divUberProduct.InnerHtml = string.Format(@"<h5>&nbsp;{0}&nbsp;&nbsp;<small>{1}</small></h5>", uberProductEstimate.Product.DisplayName, uberProductEstimate.Product.Description) +
        //                                       string.Format(@"<h5>&nbsp;<strong>{0}</strong></h5>", uberProductEstimate.EstimateInfo.Fare.Display) +
        //                                      string.Format(@"<h5>&nbsp;Pickup in {0} minutes</h5>", uberProductEstimate.EstimateInfo.PickupEstimate.ToString());
        //            divUberProduct.Attributes.Add("for", idRadioButton);


        //            HtmlGenericControl divUberImage = new HtmlGenericControl("div");
        //            divUberImage.Attributes.Add("class", "mr-4");
        //            divUberImage.Style.Add("max-width", "90px");
        //            divUberImage.Style.Add("background-image", "url('" + uberProductEstimate.Product.BackgroundImage + "')");
        //            HtmlGenericControl uberImage = new HtmlGenericControl("img");
        //            uberImage.Attributes.Add("class", "img-fluid");
        //            uberImage.Attributes.Add("src", uberProductEstimate.Product.Image);
        //            uberImage.Attributes.Add("for", idRadioButton);

        //            divUberImage.Controls.Add(uberImage);


        //            li.Controls.Add(divUberProductSelect);

        //            li.Controls.Add(divUberImage);
        //            li.Controls.Add(divUberProduct);


        //            ul.Controls.Add(li);
        //        }

        //    }


        //    divProductEstimates.Controls.Add(ul);
        //}



        #endregion




    }






}