﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Web;
using Newtonsoft.Json;


using MicrosoftTeams.Authentication.Data.Repository;
using MicrosoftTeams.Data.Models;

namespace MicrosoftTeamsTestHarness
{




    public partial class Default : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {

        }




        protected void AuthenticationRequest_Click(object sender, EventArgs e)
        {
            //Initialize Microsoft Teams Record
            MicrosoftTeamsDataRecord microsoftTeamsDataRecord;

            if (HttpContext.Current.Cache["MicrosoftTeamsDataRecord"] != null)
            {
                microsoftTeamsDataRecord = (MicrosoftTeamsDataRecord)HttpContext.Current.Cache["MicrosoftTeamsDataRecord"];
            }
            else
            {
                microsoftTeamsDataRecord = new MicrosoftTeamsDataRecord();
            }

            var scopesSettings = microsoftTeamsDataRecord.Scopes.Split(',');

            var scopes = new List<string>();
            for (int ix = 0; ix < scopesSettings.Length; ix++)
            {
                scopes.Add(scopesSettings[ix].Trim());
            }

            var microsoftTeams = new MicrosoftTeamsAuthenticationAsync(microsoftTeamsDataRecord.ClientId, microsoftTeamsDataRecord.ClientSecret);
            string authorizeUrl = microsoftTeams.GetAuthorizeUrl(scopes, null, microsoftTeamsDataRecord.RedirectUrl);

            Response.Redirect(authorizeUrl, true);
        }






        protected void SetPickupDropoff()
        {
            MicrosoftTeamsDataRecord microsoftTeamsDataRecord;

            if (HttpContext.Current.Cache["MicrosoftTeamsDataRecord"] != null)
            {
                microsoftTeamsDataRecord = (MicrosoftTeamsDataRecord)HttpContext.Current.Cache["MicrosoftTeamsDataRecord"];
            }
            else
            {
                microsoftTeamsDataRecord = new MicrosoftTeamsDataRecord();
            }


            HttpContext.Current.Cache["MicrosoftTeamsDataRecord"] = microsoftTeamsDataRecord;
        }




    }






}

